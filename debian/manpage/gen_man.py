#!/usr/bin/env python3
import shutil, fileinput, os

folderSrc = './'
folderDst = './generated/'
if not os.path.exists(folderDst):
    os.mkdir(folderDst)

fMan = open(folderSrc + 'manpage_data')
lMan = fMan.readlines()


for m in lMan:
    packNameSrc = m.split()[0].strip()
    packNameDst = folderDst + '%s.1'%(packNameSrc)
    descr = m[len(packNameSrc)+1:].strip()
    
    shutil.copyfile(folderSrc + 'manpage.1',packNameDst)
    with fileinput.FileInput(packNameDst, inplace=True) as fileR:
        for l in fileR:
            print(l.replace('%PACKAGE%', packNameSrc), end='')
    with fileinput.FileInput(packNameDst, inplace=True) as fileR:
        for l in fileR:
            print(l.replace('%DESCR%', descr), end='')



